# Introduction 
Projekt oparty na C#, zadaniem programu jest pobranie z bazy danych na komputerze klienta wszystkich potrzebnych danych i wystawienie ich do kolejki.

# Getting Started
Program przygotowujemy nie tylko pod pcMarket, tzn, żę musimy dodać w programie takie dane jak:
- Adapter - czyli wybieramy w konfiguracji z listy rozwijanej program z którego importujemy - aktualnie tylko pcmarket to musi być jak wiadomo osobny Adapter, Interfejs i model 
- W konfiguracji musi się pojawić ID sklepu, wprowadzane ręcznie
- W konfiguracji musi również pojawić się ID klienta - tutaj najlepiej jakiś hash md5 albo inny wygenerowany
ID klient i id sklepu, zawsze musi iść na początku każdego Jsona, w ten sposób będziemy rozpoznawać po stronie serwera co gdzie i z kim mamy powiązać. 
- Dla każdej kolejki - czyli dla wszystkiego co chcemy eksportować, powinien pojawić się konfiguracja czasu co ile będziemy wyrzucać plus ręczne wywołanie wszystkiego po kolei


#Ważne
W pierwszej kolejności zrób tak, żeby wszystkie dane ręcznie wpadały do kolejki i zbuduj solucję tak żebym mógł testować ze swojej strony. Staraj się robić wszędzie nazwy angielskie w kodzie.