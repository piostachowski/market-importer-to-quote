﻿using Model.DataTransfer;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Model.Domain
{
    public class SQLService
    {
        public bool CheckSQLConnection(ConfigDto config)
        {
            try
            {
                if (config.SQLSecurity == "SQL")
                {
                    var connectionString = new SqlConnectionStringBuilder();
                    connectionString["Data Source"] = config.SQLServerName;
                    connectionString["User ID"] = config.SQLUsername;
                    connectionString["Password"] = config.SQLPassword;
                    SqlConnection sc = new SqlConnection(connectionString.ConnectionString);
                    sc.Open();
                    sc.Close();
                }
                else
                {
                    var connectionString = new SqlConnectionStringBuilder();
                    connectionString["Data Source"] = config.SQLServerName;
                    connectionString.IntegratedSecurity = true;
                    SqlConnection sc = new SqlConnection(connectionString.ConnectionString);
                    sc.Open();
                    sc.Close();
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            return true;
        }


        public SqlConnection GetSQLConnection()
        {
            var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(Environment.CurrentDirectory + "\\" + "ConfigBackery");
            var connectionString = new SqlConnectionStringBuilder();

            if (config.SQLSecurity == "SQL")
            {
                connectionString["Data Source"] = config.SQLServerName;
                connectionString["User ID"] = config.SQLUsername;
                connectionString["Password"] = config.SQLPassword;
                return new SqlConnection(connectionString.ConnectionString);
            }
            connectionString["Data Source"] = config.SQLServerName;
            connectionString.IntegratedSecurity = true;
            return new SqlConnection(connectionString.ConnectionString);
        }
    }
}
