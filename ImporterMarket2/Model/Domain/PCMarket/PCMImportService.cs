﻿using Model.DataAccess.PCMarket;
using Model.DataTransfer;
using Model.DataTransfer.PCMarket;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Model.Domain.PCMarket
{
    public class PCMImportService
    {
        private readonly PCMDao _pcmDao;
        private readonly MQService _mqService;

        public PCMImportService()
        {
            _pcmDao = new PCMDao();
            _mqService = new MQService();
        }
        public void ImportTowar()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");

                var towar = new PCMBaseDto<PCMTowarDto>();
                towar.Items = _pcmDao.GetTowarList(config);

                var towarJSON = JsonConvert.SerializeObject(towar);
                _mqService.SendMqMessage(towarJSON, "PCMTowar");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }           

        }

        public void ImportAsort()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");

                var asort = new PCMBaseDto<PCMAsortDto>();
                asort.Items = _pcmDao.GetAsortList(config);

                var asortJSON = JsonConvert.SerializeObject(asort);
                _mqService.SendMqMessage(asortJSON, "PCMAsort");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ImportJM()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");

                var jm = new PCMBaseDto<PCMJMDto>();
                jm.Items = _pcmDao.GetJMList(config);

                var jMJSON = JsonConvert.SerializeObject(jm);
                _mqService.SendMqMessage(jMJSON, "PCMJM");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ImportStany()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");

                var stany = new PCMBaseDto<PCMStanDto>();
                stany.Items = _pcmDao.GetStanyList(config);

                var stanyJSON = JsonConvert.SerializeObject(stany);
                _mqService.SendMqMessage(stanyJSON, "PCMStany");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ImportDok()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");


                var doks = _pcmDao.GetDokList(config); 

                if(doks.Any())
                {
                    int i = 0;
                    int count = (doks.Count / 500) + 1;

                    while(doks.Count() > 0)
                    {
                        var dokpack = doks.Take(500).ToList();

                        var dok = new PCMBaseDto<PCMDokDto>();
                        dok.Items = dokpack;
                        dok.QueueCount = count;
                        dok.QueuePart = i;

                        var dokJSON = JsonConvert.SerializeObject(dok);
                        _mqService.SendMqMessage(dokJSON, "PCMDok");
                        if(doks.Count() > 500)
                        {
                            doks.RemoveRange(0, 500);
                        }
                        else
                        {
                            doks.RemoveRange(0, doks.Count);
                        }
                        
                        i++;
                    }
                    
                }
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ImportDokKontr()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");

                var dokkontr = new PCMBaseDto<PCMDokKontrDto>();
                dokkontr.Items = _pcmDao.GetDokKontrList(config);

                 var dokKontrJSON = JsonConvert.SerializeObject(dokkontr);
                _mqService.SendMqMessage(dokKontrJSON, "PCMDokKontr");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ImportDokKurs()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");

                var dokkurs = new PCMBaseDto<PCMDokKursDto>();
                dokkurs.Items = _pcmDao.GetDokKursList(config);


                var dokKursJSON = JsonConvert.SerializeObject(dokkurs);
                _mqService.SendMqMessage(dokKursJSON, "PCMDokKurs");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ImportTekstDok()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");

                var tekstdok = new PCMBaseDto<PCMTekstDokDto>();
                tekstdok.Items = _pcmDao.GetTekstDokList(config);

                var tekstDokJSON = JsonConvert.SerializeObject(tekstdok);
                _mqService.SendMqMessage(tekstDokJSON, "PCMTekstDok");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ImportTekstPoz()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");


                var tekstpoz = new PCMBaseDto<PCMTekstPozDto>();
                tekstpoz.Items = _pcmDao.GetTekstPozList(config);

                var tekstPozJSON = JsonConvert.SerializeObject(tekstpoz);
                _mqService.SendMqMessage(tekstPozJSON, "PCMTekstPoz");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ImportVat()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");

                var vat = new PCMBaseDto<PCMVatDto>();
                vat.Items = _pcmDao.GetVatList(config);


                var vatJSON = JsonConvert.SerializeObject(vat);
                _mqService.SendMqMessage(vatJSON, "PCMVat");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ImportUzytkownik()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");

                var uzytkownik = new PCMBaseDto<PCMUzytkownikDto>();
                uzytkownik.Items = _pcmDao.GetUzytkownikList(config);


                var uzytkownikJSON = JsonConvert.SerializeObject(uzytkownik);
                _mqService.SendMqMessage(uzytkownikJSON, "PCMUzytkownik");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ImportRozlicza()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");


                var rozlicza = new PCMBaseDto<PCMRozliczaDto>();
                rozlicza.Items = _pcmDao.GetRozliczaList(config);


                var rozliczaJSON = JsonConvert.SerializeObject(rozlicza);
                _mqService.SendMqMessage(rozliczaJSON, "PCMRozlicza");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ImportRozbicieDok()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");

                var rozbicie = new PCMBaseDto<PCMRozbicieDokDto>();
                rozbicie.Items = _pcmDao.GetRozbicieDokList(config);

                var rozbiciedokJSON = JsonConvert.SerializeObject(rozbicie);
                _mqService.SendMqMessage(rozbiciedokJSON, "PCMRozbicieDok");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        public void ImportKontrahent()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(System.Environment.CurrentDirectory + "\\" + "ConfigImporter");

                var kontrahent = new PCMBaseDto<PCMKontrahentDto>();
                kontrahent.Items = _pcmDao.GetKontrahentList(config);

                var kontrahentJSON = JsonConvert.SerializeObject(kontrahent);
                _mqService.SendMqMessage(kontrahentJSON, "PCMKontrahent");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
