﻿using Model.DataTransfer;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Domain
{
    public class MQService
    {
        public void SendMqMessage(string msg, string queueName)
        {
            var ConfigFile = System.Environment.CurrentDirectory + "\\" + "ConfigImporter";
            var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(ConfigFile);

            var conn = ConfigWriter.CreateMQConnection(config);


            using (var connection = conn.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: queueName,
                                     durable: true,
                                     exclusive: false,
                                     autoDelete: false,
                                     arguments: null);

                string message = msg;
                var body = Encoding.UTF8.GetBytes(message);

                channel.BasicPublish(exchange: "",
                                     routingKey: queueName,
                                     basicProperties: null,
                                     body: body);
                Console.WriteLine(" [x] Sent.");
            }
        }
    }
}
