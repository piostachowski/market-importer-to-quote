﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMDokKursDto
    {
        public decimal DokId { get; set; }
        public int Znaczenie { get; set; }
        public decimal WalId { get; set; }
        public decimal Kurs { get; set; }
        public DateTime Data { get; set; }
        public string NrTabeli { get; set; }
        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }
    }
}
