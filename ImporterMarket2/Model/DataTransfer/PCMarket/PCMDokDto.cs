﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMDokDto
    {
        public decimal DokId { get; set; }
        public decimal UzId { get; set; }
        public decimal MagId { get; set; }
        public DateTime Data { get; set; }
        public int KolejnyWDniu { get; set; }
        public DateTime DataDod { get; set; }
        public DateTime DataPom { get; set; }
        public string NrDok { get; set; }
        public int TypDok { get; set; }
        public int Aktywny { get; set; }
        public int Opcja1 { get; set; }
        public int Opcja2 { get; set; }
        public int Opcja3 { get; set; }
        public int Opcja4 { get; set; }
        public int CenyZakBrutto { get; set; }
        public int CenySpBrutto { get; set; }
        public int FormaPlat { get; set; }
        public int TerminPlat { get; set; }
        public int PoziomCen { get; set; }
        public decimal RabatProc { get; set; }
        public decimal Netto { get; set; }
        public decimal NettoDet { get; set; }
        public decimal PodatekDet { get; set; }
        public decimal NettoDetUslugi { get; set; }
        public decimal PodatekDetUslugi { get; set; }
        public decimal NettoMag { get; set; }
        public decimal PodatekMag { get; set; }
        public decimal NettoMagUslugi { get; set; }
        public decimal PodatekMagUslugi { get; set; }
        public decimal Razem { get; set; }
        public decimal DoZaplaty { get; set; }
        public decimal Zaplacono { get; set; }
        public int EksportFK { get; set; }
        public DateTime Zmiana { get; set; }
        public int NrKolejny { get; set; }
        public int NrKolejnyMag { get; set; }
        public decimal Kwota11 { get; set; }
        public decimal Kwota12 { get; set; }
        public decimal WalId { get; set; }
        public decimal Kurs { get; set; }
        public decimal Podatek { get; set; }
        public decimal NettoUslugi { get; set; }
        public decimal PodatekUslugi { get; set; }
        public List<PCMPozDokDto> PozDok { get; set; }


        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }

    }
}
