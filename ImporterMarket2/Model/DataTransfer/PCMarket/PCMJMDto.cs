﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMJMDto
    {
        public decimal JMId { get; set; }
        public string Nazwa { get; set; }
        public int Precyzja { get; set; }
        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }
    }
}
