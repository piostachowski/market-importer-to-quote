﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMTekstDokDto
    {
        public decimal DokId { get; set; }
        public int Znaczenie { get; set; }
        public string Tekst { get; set; }
        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }
    }
}
