﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMDokKontrDto
    {
        public decimal DokId { get; set; }
        public decimal KontrId { get; set; }
        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }
    }
}
