﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMRozbicieDokDto
    {
        public decimal DokId { get; set; }
        public int Stawka { get; set; }
        public decimal Netto { get; set; }
        public decimal Podatek { get; set; }
        public decimal Brutto { get; set; }
        public decimal NettoDet { get; set; }
        public decimal PodatekDet { get; set; }
        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }

        public decimal SumaWartosciNetto { get; set; }
        public decimal SumaWartosciPodatku { get; set; }
        public decimal SumaWartosciBrutto { get; set; }
    }
}
