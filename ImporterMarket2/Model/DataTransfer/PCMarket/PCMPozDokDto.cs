﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMPozDokDto
    {
        public decimal DokId { get; set; }
        public int Kolejnosc { get; set; }
        public int NrPozycji { get; set; }
        public decimal TowId { get; set; }
        public int TypPoz { get; set; }
        public decimal IloscPlus { get; set; }
        public decimal IloscMinus { get; set; }
        public int PoziomCen { get; set; }
        public int Metoda { get; set; }
        public decimal CenaDomyslna { get; set; }
        public decimal CenaPrzedRab { get; set; }
        public decimal RabatProc { get; set; }
        public decimal Wartosc { get; set; }
        public decimal CenaDet { get; set; }
        public decimal CenaMag { get; set; }
        public int Stawka { get; set; }
        public int TypTowaru { get; set; }
        public decimal IleWZgrzewce { get; set; }
        public int StawkaDod { get; set; }
        public decimal Netto { get; set; }
        public decimal Podatek { get; set; }
        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }
    }
}
