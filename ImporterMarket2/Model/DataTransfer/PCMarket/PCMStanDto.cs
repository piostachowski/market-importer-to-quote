﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMStanDto
    {
        public decimal IstwId { get; set; }
        public decimal StanMag { get; set; }
        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }
    }
}
