﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMUzytkownikDto
    {
        public decimal UzId { get; set; }
        public string Identyfikator { get; set; }
        public string Nazwisko { get; set; }
        public int Aktywny { get; set; }
        public int NrKasjera { get; set; }
        public int Rola { get; set; }
        public string KodKasjera { get; set; }
        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }
    }
}
