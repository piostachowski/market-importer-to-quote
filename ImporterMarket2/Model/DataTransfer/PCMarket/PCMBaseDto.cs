﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMBaseDto<T>
    {       
        public int QueuePart { get; set; }
        public int QueueCount { get; set; }
        public List<T> Items { get; set; }
    }
}
