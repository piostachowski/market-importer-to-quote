﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMRozliczaDto
    {
        public decimal DokId { get; set; }
        public int Kolejnosc { get; set; }
        public decimal RozliczanyDokId { get; set; }
        public decimal Rozliczenie { get; set; }
        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }
    }
}
