﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMKontrahentDto
    {
        public decimal KontrId { get; set; }
        public decimal AkwId { get; set; }
        public string Nazwa { get; set; }
        public string NaPrzelewie1 { get; set; }
        public string NaPrzelewie2 { get; set; }
        public string Skrot { get; set; }
        public string Ulica { get; set; }
        public string Kod { get; set; }
        public string Miasto { get; set; }
        public string Telefon { get; set; }
        public string Fax { get; set; }
        public string EMail { get; set; }
        public string Bank { get; set; }
        public string Konto { get; set; }
        public string NIP { get; set; }
        public int Staly { get; set; }
        public int Dostawca { get; set; }
        public decimal RabatDost { get; set; }
        public int FormaPlatDost { get; set; }
        public int TermPlatDost { get; set; }
        public int CzasRealZam { get; set; }
        public int Producent { get; set; }
        public int Odbiorca { get; set; }
        public decimal RabatOdb { get; set; }
        public int FormaPlatOdb { get; set; }
        public int TermPlatOdb { get; set; }
        public decimal MaxKredyt { get; set; }
        public int MaxPoTermPlat { get; set; }
        public string KodKarty { get; set; }
        public int KartaAktywna { get; set; }
        public DateTime TermWaznKarty { get; set; }
        public int PoziomRabatu { get; set; }
        public string NrAnalityki { get; set; }
        public string KodKontr { get; set; }
        public decimal BazaKontrId { get; set; }
        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }
    }
}
