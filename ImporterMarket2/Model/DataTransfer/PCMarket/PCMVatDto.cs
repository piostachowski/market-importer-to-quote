﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMVatDto
    {
        public int Stawka { get; set; }
        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }
    }
}
