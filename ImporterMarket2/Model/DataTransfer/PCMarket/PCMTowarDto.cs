﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer.PCMarket
{
    public class PCMTowarDto
    {
        public decimal TowId { get; set; }
        public decimal AsId { get; set; }
        public decimal JMId { get; set; }
        public string Nazwa { get; set; }
        public string Skrot { get; set; }
        public string Kod { get; set; }
        public decimal Marza { get; set; }
        public decimal HurtRabat { get; set; }
        public decimal NocNarzut { get; set; }
        public decimal CenaEw { get; set; }
        public decimal CenaDet { get; set; }
        public decimal CenaHurt { get; set; }
        public decimal CenaNoc { get; set; }
        public decimal CenaDod { get; set; }
        public int Stawka { get; set; }
        public int Aktywny { get; set; }
        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }

    }
}
