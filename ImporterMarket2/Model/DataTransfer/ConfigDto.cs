﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.DataTransfer
{
    public class ConfigDto
    {
        #region RabbitMQ

        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Pasword { get; set; }
        public string VirtualHost { get; set; }
        public int Port { get; set; }
        public int RequestedConnectionTimeout { get; set; }
        public int RequestedHeartbeat { get; set; }
        public bool AutomaticRecoveryEnabled { get; set; }

        #endregion
        #region MSSQL

        public string SQLServerName { get; set; }
        public string SQLName { get; set; }
        public string SQLUsername { get; set; }
        public string SQLPassword { get; set; }
        public string SQLSecurity { get; set; }

        #endregion

        public string ShopNumber { get; set; }
        public string CompanyId { get; set; }
    }
}
