﻿using Model.DataTransfer;
using Model.DataTransfer.PCMarket;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace Model.DataAccess.PCMarket
{
    public class PCMDao
    {
        public List<PCMTowarDto> GetTowarList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMTowarDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if(config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }
            
            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                SqlCommand command = new SqlCommand("Select TowId, AsId, JMId, Nazwa, Skrot, Kod, Marza, HurtRabat, NocNarzut, CenaEw, CenaDet, CenaHurt, CenaNoc, CenaDod, Stawka, Aktywny,(TowId) as BazaTowId,(" + idBazy + ") as BazaId from Towar", connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMTowarDto()
                                {
                                    TowId = reader.IsDBNull(reader.GetOrdinal("TowId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("TowId")),
                                    AsId = reader.IsDBNull(reader.GetOrdinal("AsId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("AsId")),
                                    JMId = reader.IsDBNull(reader.GetOrdinal("JMId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("JMId")),
                                    Nazwa = reader.IsDBNull(reader.GetOrdinal("Nazwa")) ? "" : reader.GetString(reader.GetOrdinal("Nazwa")),
                                    Skrot = reader.IsDBNull(reader.GetOrdinal("Skrot")) ? "" : reader.GetString(reader.GetOrdinal("Skrot")),
                                    Kod = reader.IsDBNull(reader.GetOrdinal("Kod")) ? "" : reader.GetString(reader.GetOrdinal("Kod")),
                                    Marza = reader.IsDBNull(reader.GetOrdinal("Marza")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Marza")),
                                    HurtRabat = reader.IsDBNull(reader.GetOrdinal("HurtRabat")) ? 0 : reader.GetDecimal(reader.GetOrdinal("HurtRabat")),
                                    NocNarzut = reader.IsDBNull(reader.GetOrdinal("NocNarzut")) ? 0 : reader.GetDecimal(reader.GetOrdinal("NocNarzut")),
                                    CenaEw = reader.IsDBNull(reader.GetOrdinal("CenaEw")) ? 0 : reader.GetDecimal(reader.GetOrdinal("CenaEw")),
                                    CenaDet = reader.IsDBNull(reader.GetOrdinal("CenaDet")) ? 0 : reader.GetDecimal(reader.GetOrdinal("CenaDet")),
                                    CenaHurt = reader.IsDBNull(reader.GetOrdinal("CenaHurt")) ? 0 : reader.GetDecimal(reader.GetOrdinal("CenaHurt")),
                                    CenaNoc = reader.IsDBNull(reader.GetOrdinal("CenaNoc")) ? 0 : reader.GetDecimal(reader.GetOrdinal("CenaNoc")),
                                    CenaDod = reader.IsDBNull(reader.GetOrdinal("CenaDod")) ? 0 : reader.GetDecimal(reader.GetOrdinal("CenaDod")),
                                    Stawka = reader.IsDBNull(reader.GetOrdinal("Stawka")) ? 0 : reader.GetInt16(reader.GetOrdinal("Stawka")),
                                    Aktywny = reader.IsDBNull(reader.GetOrdinal("Aktywny")) ? 0 : reader.GetInt16(reader.GetOrdinal("Aktywny")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId
                                });
                            }

                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {

                }
                connection.Close();
            }


            return result;
        }

        public List<PCMAsortDto> GetAsortList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMAsortDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                SqlCommand command = new SqlCommand("Select AsId, Nazwa,(AsId) as BazaAsId, Marza, OpcjaMarzy, HurtRabat, OpcjaRabatu, NocNarzut, OpcjaNarzutu, CentrAsId, UkrytyNaPanelach, UkrytyNaBonowniku, BezAutoEtykiet, Param1 from Asort", connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMAsortDto()
                                {
                                    AsId = reader.IsDBNull(reader.GetOrdinal("AsId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("AsId")),
                                    Nazwa = reader.IsDBNull(reader.GetOrdinal("Nazwa")) ? "" : reader.GetString(reader.GetOrdinal("Nazwa")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId
                                });
                            }
                        }
                        catch (Exception ex)
                        {
                            
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                connection.Close();
            }


            return result;
        }

        public List<PCMJMDto> GetJMList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMJMDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                SqlCommand command = new SqlCommand("Select JMId, Nazwa, Precyzja,(JMId) as BazaJMId,(" + idBazy + ")as BazaId from JM", connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMJMDto()
                                {
                                    JMId = reader.IsDBNull(reader.GetOrdinal("JMId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("JMId")),
                                    Nazwa = reader.IsDBNull(reader.GetOrdinal("Nazwa")) ? "" : reader.GetString(reader.GetOrdinal("Nazwa")),
                                    Precyzja = reader.IsDBNull(reader.GetOrdinal("Precyzja")) ? 0 : reader.GetInt16(reader.GetOrdinal("Precyzja")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId
                                });
                            }

                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {

                }
                connection.Close();
            }


            return result;
        }

        public List<PCMStanDto> GetStanyList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMStanDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                SqlCommand command = new SqlCommand("Select (TowId) as IstwId,(TowId)as BazaTowId, StanMag,(" + idBazy + ")as BazaId  from Istw", connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMStanDto()
                                {
                                    IstwId = reader.IsDBNull(reader.GetOrdinal("IstwId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("IstwId")),
                                    StanMag = reader.IsDBNull(reader.GetOrdinal("StanMag")) ? 0 : reader.GetDecimal(reader.GetOrdinal("StanMag")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId
                                });
                            }

                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {

                }
                connection.Close();
            }


            return result;
        }

        public List<PCMDokDto> GetDokList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMDokDto>();
            List<int> dokIds;

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            connStringIn.ConnectTimeout = 0;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {                
                var queryDok = @"Select DokId,UzId,MagId,Data,KolejnyWDniu,DataDod,DataPom,NrDok,TypDok,Aktywny,Opcja1,Opcja2,Opcja3,Opcja4,
                                CenyZakBrutto,CenySpBrutto,FormaPlat,TerminPlat,PoziomCen,RabatProc,Netto,Podatek,NettoUslugi,PodatekUslugi,
                                NettoDet,PodatekDet,NettoDetUslugi,PodatekDetUslugi,NettoMag,PodatekMag,NettoMagUslugi,PodatekMagUslugi,
                                Razem,DoZaplaty,Zaplacono,EksportFk,Zmiana,NrKolejny,NrKolejnyMag,Kwota11,Kwota12,Walid,Kurs,
                                (DokId) as BazaDokId,(" + idBazy + ") as BazaId  from Dok where Data > (DATEADD(dd, 0, DATEDIFF(dd, 732, GETDATE())))";
                SqlCommand command = new SqlCommand(queryDok, connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMDokDto()
                                {
                                    DokId = reader.IsDBNull(reader.GetOrdinal("DokId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("DokId")),
                                    UzId = reader.IsDBNull(reader.GetOrdinal("UzId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("UzId")),
                                    MagId = reader.IsDBNull(reader.GetOrdinal("MagId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("MagId")),
                                    Data = reader.IsDBNull(reader.GetOrdinal("Data")) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("Data")),
                                    KolejnyWDniu = reader.IsDBNull(reader.GetOrdinal("KolejnyWDniu")) ? 0 : reader.GetInt32(reader.GetOrdinal("KolejnyWDniu")),
                                    DataDod = reader.IsDBNull(reader.GetOrdinal("DataDod")) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("DataDod")),
                                    DataPom = reader.IsDBNull(reader.GetOrdinal("DataPom")) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("DataPom")),
                                    NrDok = reader.IsDBNull(reader.GetOrdinal("NrDok")) ? "" : reader.GetString(reader.GetOrdinal("NrDok")),
                                    TypDok = reader.IsDBNull(reader.GetOrdinal("TypDok")) ? 0 : reader.GetInt16(reader.GetOrdinal("TypDok")),
                                    Aktywny = reader.IsDBNull(reader.GetOrdinal("Aktywny")) ? 0 : reader.GetInt16(reader.GetOrdinal("Aktywny")),
                                    Opcja1 = reader.IsDBNull(reader.GetOrdinal("Opcja1")) ? 0 : reader.GetInt16(reader.GetOrdinal("Opcja1")),
                                    Opcja2 = reader.IsDBNull(reader.GetOrdinal("Opcja2")) ? 0 : reader.GetInt16(reader.GetOrdinal("Opcja2")),
                                    Opcja3 = reader.IsDBNull(reader.GetOrdinal("Opcja3")) ? 0 : reader.GetInt16(reader.GetOrdinal("Opcja3")),
                                    Opcja4 = reader.IsDBNull(reader.GetOrdinal("Opcja4")) ? 0 : reader.GetInt16(reader.GetOrdinal("Opcja4")),
                                    CenyZakBrutto = reader.IsDBNull(reader.GetOrdinal("CenyZakBrutto")) ? 0 : reader.GetInt16(reader.GetOrdinal("CenyZakBrutto")),
                                    CenySpBrutto = reader.IsDBNull(reader.GetOrdinal("CenySpBrutto")) ? 0 : reader.GetInt16(reader.GetOrdinal("CenySpBrutto")),
                                    FormaPlat = reader.IsDBNull(reader.GetOrdinal("FormaPlat")) ? 0 : reader.GetInt16(reader.GetOrdinal("FormaPlat")),
                                    TerminPlat = reader.IsDBNull(reader.GetOrdinal("TerminPlat")) ? 0 : reader.GetInt16(reader.GetOrdinal("TerminPlat")),
                                    PoziomCen = reader.IsDBNull(reader.GetOrdinal("PoziomCen")) ? 0 : reader.GetInt16(reader.GetOrdinal("PoziomCen")),
                                    RabatProc = reader.IsDBNull(reader.GetOrdinal("RabatProc")) ? 0 : reader.GetDecimal(reader.GetOrdinal("RabatProc")),
                                    Netto = reader.IsDBNull(reader.GetOrdinal("Netto")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Netto")),
                                    Podatek = reader.IsDBNull(reader.GetOrdinal("Podatek")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Podatek")),
                                    NettoUslugi = reader.IsDBNull(reader.GetOrdinal("NettoUslugi")) ? 0 : reader.GetDecimal(reader.GetOrdinal("NettoUslugi")),
                                    PodatekUslugi = reader.IsDBNull(reader.GetOrdinal("PodatekUslugi")) ? 0 : reader.GetDecimal(reader.GetOrdinal("PodatekUslugi")),
                                    NettoDet = reader.IsDBNull(reader.GetOrdinal("NettoDet")) ? 0 : reader.GetDecimal(reader.GetOrdinal("NettoDet")),
                                    PodatekDet = reader.IsDBNull(reader.GetOrdinal("PodatekDet")) ? 0 : reader.GetDecimal(reader.GetOrdinal("PodatekDet")),
                                    NettoDetUslugi = reader.IsDBNull(reader.GetOrdinal("NettoDetUslugi")) ? 0 : reader.GetDecimal(reader.GetOrdinal("NettoDetUslugi")),
                                    PodatekDetUslugi = reader.IsDBNull(reader.GetOrdinal("PodatekDetUslugi")) ? 0 : reader.GetDecimal(reader.GetOrdinal("PodatekDetUslugi")),
                                    NettoMag = reader.IsDBNull(reader.GetOrdinal("NettoMag")) ? 0 : reader.GetDecimal(reader.GetOrdinal("NettoMag")),
                                    PodatekMag = reader.IsDBNull(reader.GetOrdinal("PodatekMag")) ? 0 : reader.GetDecimal(reader.GetOrdinal("PodatekMag")),
                                    NettoMagUslugi = reader.IsDBNull(reader.GetOrdinal("NettoMagUslugi")) ? 0 : reader.GetDecimal(reader.GetOrdinal("NettoMagUslugi")),
                                    PodatekMagUslugi = reader.IsDBNull(reader.GetOrdinal("PodatekMagUslugi")) ? 0 : reader.GetDecimal(reader.GetOrdinal("PodatekMagUslugi")),
                                    Razem = reader.IsDBNull(reader.GetOrdinal("Razem")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Razem")),
                                    DoZaplaty = reader.IsDBNull(reader.GetOrdinal("DoZaplaty")) ? 0 : reader.GetDecimal(reader.GetOrdinal("DoZaplaty")),
                                    Zaplacono = reader.IsDBNull(reader.GetOrdinal("Zaplacono")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Zaplacono")),
                                    EksportFK = reader.IsDBNull(reader.GetOrdinal("EksportFK")) ? 0 : reader.GetInt16(reader.GetOrdinal("EksportFK")),
                                    Zmiana = reader.IsDBNull(reader.GetOrdinal("Zmiana")) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("Zmiana")),
                                    NrKolejny = reader.IsDBNull(reader.GetOrdinal("NrKolejny")) ? 0 : reader.GetInt32(reader.GetOrdinal("NrKolejny")),
                                    NrKolejnyMag = reader.IsDBNull(reader.GetOrdinal("NrKolejnyMag")) ? 0 : reader.GetInt32(reader.GetOrdinal("NrKolejnyMag")),
                                    Kwota11 = reader.IsDBNull(reader.GetOrdinal("Kwota11")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Kwota11")),
                                    Kwota12 = reader.IsDBNull(reader.GetOrdinal("Kwota12")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Kwota12")),
                                    WalId = reader.IsDBNull(reader.GetOrdinal("Walid")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Walid")),
                                    Kurs = reader.IsDBNull(reader.GetOrdinal("Kurs")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Kurs")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId

                                });
                            }

                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                connection.Close();
            }

            if (result.Any())
            {
                dokIds = result.Select(d => (int)d.DokId).ToList();
                //get pozdoks in packs
                while (dokIds.Count() > 0)
                {
                    var dokIdsPack = dokIds.Take(500).ToList();
                    var pozdoks = GetPozDokList(config, dokIdsPack);
                    if (pozdoks.Any())
                    {
                        foreach (var dokId in dokIdsPack)
                        {
                            result.Where(d => d.DokId == (decimal)dokId).First().PozDok = pozdoks.Where(p => p.DokId == (decimal)dokId).ToList();
                        }
                    }
                    if (dokIds.Count() > 500)
                    {
                        dokIds.RemoveRange(0, 500);
                    }
                    else
                    {
                        dokIds.RemoveRange(0, dokIds.Count);
                    }
                }




                
            }

            return result;
        }

        public List<PCMPozDokDto> GetPozDokList(ConfigDto config, List<int> dokIds)
        {
            int idBazy = 0;
            var result = new List<PCMPozDokDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            connStringIn.ConnectTimeout = 0;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                var queryPozDok = @"Select DokId,Kolejnosc,NrPozycji,TowId,TypPoz,IloscPlus,IloscMinus,PoziomCen,Metoda,CenaDomyslna,CenaPrzedRab,
                                    RabatProc,CenaPoRab,Wartosc,CenaDet,CenaMag,Stawka,TypTowaru,IleWZgrzewce,StawkaDod,Netto,Podatek,
                                    (DokId) as BazaDokId,(" + idBazy + ") as BazaId  from PozDok where DokId IN (" + string.Join<int>(",", dokIds) + ")";


                SqlCommand command = new SqlCommand(queryPozDok, connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMPozDokDto()
                                {
                                    DokId = reader.IsDBNull(reader.GetOrdinal("DokId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("DokId")),
                                    Kolejnosc = reader.IsDBNull(reader.GetOrdinal("Kolejnosc")) ? 0 : reader.GetInt32(reader.GetOrdinal("Kolejnosc")),
                                    NrPozycji = reader.IsDBNull(reader.GetOrdinal("NrPozycji")) ? 0 : reader.GetInt32(reader.GetOrdinal("NrPozycji")),
                                    TowId = reader.IsDBNull(reader.GetOrdinal("TowId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("TowId")),
                                    TypPoz = reader.IsDBNull(reader.GetOrdinal("TypPoz")) ? 0 : reader.GetInt16(reader.GetOrdinal("TypPoz")),
                                    IloscPlus = reader.IsDBNull(reader.GetOrdinal("IloscPlus")) ? 0 : reader.GetDecimal(reader.GetOrdinal("IloscPlus")),
                                    IloscMinus = reader.IsDBNull(reader.GetOrdinal("IloscMinus")) ? 0 : reader.GetDecimal(reader.GetOrdinal("IloscMinus")),
                                    PoziomCen = reader.IsDBNull(reader.GetOrdinal("PoziomCen")) ? 0 : reader.GetInt16(reader.GetOrdinal("PoziomCen")),
                                    Metoda = reader.IsDBNull(reader.GetOrdinal("Metoda")) ? 0 : reader.GetInt16(reader.GetOrdinal("Metoda")),
                                    CenaDomyslna = reader.IsDBNull(reader.GetOrdinal("CenaDomyslna")) ? 0 : reader.GetDecimal(reader.GetOrdinal("CenaDomyslna")),
                                    CenaPrzedRab = reader.IsDBNull(reader.GetOrdinal("CenaPrzedRab")) ? 0 : reader.GetDecimal(reader.GetOrdinal("CenaPrzedRab")),
                                    RabatProc = reader.IsDBNull(reader.GetOrdinal("RabatProc")) ? 0 : reader.GetDecimal(reader.GetOrdinal("RabatProc")),
                                    Wartosc = reader.IsDBNull(reader.GetOrdinal("Wartosc")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Wartosc")),
                                    CenaDet = reader.IsDBNull(reader.GetOrdinal("CenaDet")) ? 0 : reader.GetDecimal(reader.GetOrdinal("CenaDet")),
                                    CenaMag = reader.IsDBNull(reader.GetOrdinal("CenaMag")) ? 0 : reader.GetDecimal(reader.GetOrdinal("CenaMag")),
                                    Stawka = reader.IsDBNull(reader.GetOrdinal("Stawka")) ? 0 : reader.GetInt16(reader.GetOrdinal("Stawka")),
                                    TypTowaru = reader.IsDBNull(reader.GetOrdinal("TypTowaru")) ? 0 : reader.GetInt16(reader.GetOrdinal("TypTowaru")),
                                    IleWZgrzewce = reader.IsDBNull(reader.GetOrdinal("IleWZgrzewce")) ? 0 : reader.GetDecimal(reader.GetOrdinal("IleWZgrzewce")),
                                    StawkaDod = reader.IsDBNull(reader.GetOrdinal("StawkaDod")) ? 0 : reader.GetInt16(reader.GetOrdinal("StawkaDod")),
                                    Netto = reader.IsDBNull(reader.GetOrdinal("Netto")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Netto")),
                                    Podatek = reader.IsDBNull(reader.GetOrdinal("Podatek")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Podatek")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId
                                });
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {

                }
                connection.Close();
            }


            return result;
        }

        public List<PCMDokKontrDto> GetDokKontrList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMDokKontrDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                var queryDokKontr = "Select DokId,KontrId,(DokId) as BazaDokId,(" + idBazy + ") as BazaId  from DokKontr ";
                SqlCommand command = new SqlCommand(queryDokKontr, connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMDokKontrDto()
                                {
                                    DokId = reader.IsDBNull(reader.GetOrdinal("DokId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("DokId")),
                                    KontrId = reader.IsDBNull(reader.GetOrdinal("KontrId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("KontrId")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId

                                });
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                connection.Close();
            }


            return result;
        }

        public List<PCMDokKursDto> GetDokKursList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMDokKursDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                var queryDokKurs = "Select DokId,Znaczenie,WalId,Kurs,Data,NrTabeli,(DokId) as BazaDokId,(" + idBazy + ") as BazaId  from DokKurs";
                SqlCommand command = new SqlCommand(queryDokKurs, connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMDokKursDto()
                                {
                                    DokId = reader.IsDBNull(reader.GetOrdinal("DokId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("DokId")),
                                    Znaczenie = reader.IsDBNull(reader.GetOrdinal("Znaczenie")) ? 0 : reader.GetInt16(reader.GetOrdinal("Znaczenie")),
                                    WalId = reader.IsDBNull(reader.GetOrdinal("WalId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("WalId")),
                                    Kurs = reader.IsDBNull(reader.GetOrdinal("Kurs")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Kurs")),
                                    Data = reader.IsDBNull(reader.GetOrdinal("Data")) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("Data")),
                                    NrTabeli = reader.IsDBNull(reader.GetOrdinal("NrTabeli")) ? "" : reader.GetString(reader.GetOrdinal("NrTabeli")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId

                                });
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                connection.Close();
            }


            return result;
        }

        public List<PCMTekstDokDto> GetTekstDokList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMTekstDokDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                var queryTekstdok = "Select DokId,Znaczenie,Tekst,(DokId) as BazaDokId,(" + idBazy + ") as BazaId  from TekstDok ";
                SqlCommand command = new SqlCommand(queryTekstdok, connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMTekstDokDto()
                                {
                                    DokId = reader.IsDBNull(reader.GetOrdinal("DokId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("DokId")),
                                    Znaczenie = reader.IsDBNull(reader.GetOrdinal("Znaczenie")) ? 0 : reader.GetInt16(reader.GetOrdinal("Znaczenie")),
                                    Tekst = reader.IsDBNull(reader.GetOrdinal("Tekst")) ? "" : reader.GetString(reader.GetOrdinal("Tekst")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId
                                });
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                connection.Close();
            }


            return result;
        }

        public List<PCMTekstPozDto> GetTekstPozList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMTekstPozDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                var queryTekstPoz = "Select DokId,Kolejnosc,Znaczenie,Tekst,(DokId) as BazaDokId,(" + idBazy + ") as BazaId  from TekstPoz ";
                SqlCommand command = new SqlCommand(queryTekstPoz, connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMTekstPozDto()
                                {
                                    DokId = reader.IsDBNull(reader.GetOrdinal("DokId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("DokId")),
                                    Kolejnosc = reader.IsDBNull(reader.GetOrdinal("Kolejnosc")) ? 0 : reader.GetInt32(reader.GetOrdinal("Kolejnosc")),
                                    Znaczenie = reader.IsDBNull(reader.GetOrdinal("Znaczenie")) ? 0 : reader.GetInt16(reader.GetOrdinal("Znaczenie")),
                                    Tekst = reader.IsDBNull(reader.GetOrdinal("Tekst")) ? "" : reader.GetString(reader.GetOrdinal("Tekst")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId
                                });
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                connection.Close();
            }


            return result;
        }

        public List<PCMVatDto> GetVatList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMVatDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                var queryVat = "Select Stawka," + idBazy + " as BazaId  from Vat";
                SqlCommand command = new SqlCommand(queryVat, connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMVatDto()
                                {
                                    Stawka = reader.IsDBNull(reader.GetOrdinal("Stawka")) ? 0 : reader.GetInt16(reader.GetOrdinal("Stawka")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId
                                });
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                connection.Close();
            }


            return result;
        }

        public List<PCMUzytkownikDto> GetUzytkownikList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMUzytkownikDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                var queryUzytkownik = "Select UzId,Identyfikator,Nazwisko,Aktywny,NrKasjera,Rola,KodKasjera,(UzId) as BazaUzId,(" + idBazy + ") as BazaId  from Uzytkownik";
                SqlCommand command = new SqlCommand(queryUzytkownik, connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMUzytkownikDto()
                                {
                                    UzId = reader.IsDBNull(reader.GetOrdinal("UzId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("UzId")),
                                    Identyfikator = reader.IsDBNull(reader.GetOrdinal("Identyfikator")) ? "" : reader.GetString(reader.GetOrdinal("Identyfikator")),
                                    Nazwisko = reader.IsDBNull(reader.GetOrdinal("Nazwisko")) ? "" : reader.GetString(reader.GetOrdinal("Nazwisko")),
                                    Aktywny = reader.IsDBNull(reader.GetOrdinal("Aktywny")) ? 0 : reader.GetInt16(reader.GetOrdinal("Aktywny")),
                                    NrKasjera = reader.IsDBNull(reader.GetOrdinal("NrKasjera")) ? 0 : reader.GetInt16(reader.GetOrdinal("NrKasjera")),
                                    Rola = reader.IsDBNull(reader.GetOrdinal("Rola")) ? 0 : reader.GetInt16(reader.GetOrdinal("Rola")),
                                    KodKasjera = reader.IsDBNull(reader.GetOrdinal("KodKasjera")) ? "" : reader.GetString(reader.GetOrdinal("KodKasjera")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId
                                });
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                connection.Close();
            }


            return result;
        }

        public List<PCMRozliczaDto> GetRozliczaList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMRozliczaDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                var queryRozlicza = "Select DokId,Kolejnosc,RozliczanyDokId,Rozliczenie,(DokId) as BazaDokId,(" + idBazy + ") as BazaId  from Rozlicza";
                SqlCommand command = new SqlCommand(queryRozlicza, connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMRozliczaDto()
                                {
                                    DokId = reader.IsDBNull(reader.GetOrdinal("DokId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("DokId")),
                                    Kolejnosc = reader.IsDBNull(reader.GetOrdinal("Kolejnosc")) ? 0 : reader.GetInt32(reader.GetOrdinal("Kolejnosc")),
                                    RozliczanyDokId = reader.IsDBNull(reader.GetOrdinal("RozliczanyDokId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("RozliczanyDokId")),
                                    Rozliczenie = reader.IsDBNull(reader.GetOrdinal("Rozliczenie")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Rozliczenie")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId
                                });
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                connection.Close();
            }


            return result;
        }

        public List<PCMRozbicieDokDto> GetRozbicieDokList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMRozbicieDokDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                var queryRozbicieDok = "Select DokId,Stawka,Netto,Podatek,NettoDet,PodatekDet,(DokId) as BazaDokId,(" + idBazy + ") as BazaId  from RozbicieDok";
                SqlCommand command = new SqlCommand(queryRozbicieDok, connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMRozbicieDokDto()
                                {
                                    DokId = reader.IsDBNull(reader.GetOrdinal("DokId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("DokId")),
                                    Stawka = reader.IsDBNull(reader.GetOrdinal("Stawka")) ? 0 : reader.GetInt16(reader.GetOrdinal("Stawka")),
                                    Netto = reader.IsDBNull(reader.GetOrdinal("Netto")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Netto")),
                                    Podatek = reader.IsDBNull(reader.GetOrdinal("Podatek")) ? 0 : reader.GetDecimal(reader.GetOrdinal("Podatek")),
                                    NettoDet = reader.IsDBNull(reader.GetOrdinal("NettoDet")) ? 0 : reader.GetDecimal(reader.GetOrdinal("NettoDet")),
                                    PodatekDet = reader.IsDBNull(reader.GetOrdinal("PodatekDet")) ? 0 : reader.GetDecimal(reader.GetOrdinal("PodatekDet")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId
                                });
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                connection.Close();
            }


            return result;
        }

        public List<PCMKontrahentDto> GetKontrahentList(ConfigDto config)
        {
            int idBazy = 0;
            var result = new List<PCMKontrahentDto>();

            var connStringIn = new SqlConnectionStringBuilder();
            connStringIn["Data Source"] = config.SQLServerName;
            connStringIn["Initial Catalog"] = config.SQLName;
            if (config.SQLSecurity == "SQL")
            {
                connStringIn["User ID"] = config.SQLUsername;
                connStringIn["Password"] = config.SQLPassword;
            }
            else
            {
                connStringIn["Integrated Security"] = true;
            }

            using (SqlConnection connection = new SqlConnection(connStringIn.ConnectionString))
            {
                var queryKontrahent = @"Select KontrId,AkwId,Nazwa,NaPrzelewie1,NaPrzelewie2,Skrot,Ulica,Kod,Miasto,Telefon,Fax,EMail,
                                    Bank,Konto,NIP,Staly,Dostawca,RabatDost,FormaPlatDost,TermPlatDost,CzasRealZam,Producent,Odbiorca,RabatOdb,FormaPlatOdb,
                                    TermPlatOdb,MaxKredyt,MaxPoTermPlat,KodKarty,KartaAktywna,TermWaznKarty,PoziomRabatu,NrAnalityki,KodKontr,Aktywny,Zmiana,
                                    (KontrId) as BazaKontrId,(" + idBazy + ") as BazaId  from Kontrahent";
                SqlCommand command = new SqlCommand(queryKontrahent, connection);
                connection.Open();
                try
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        try
                        {
                            while (reader.Read())
                            {
                                result.Add(new PCMKontrahentDto()
                                {
                                    KontrId = reader.IsDBNull(reader.GetOrdinal("KontrId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("KontrId")),
                                    AkwId = reader.IsDBNull(reader.GetOrdinal("AkwId")) ? 0 : reader.GetDecimal(reader.GetOrdinal("AkwId")),
                                    Nazwa = reader.IsDBNull(reader.GetOrdinal("Nazwa")) ? "" : reader.GetString(reader.GetOrdinal("Nazwa")),
                                    NaPrzelewie1 = reader.IsDBNull(reader.GetOrdinal("NaPrzelewie1")) ? "" : reader.GetString(reader.GetOrdinal("NaPrzelewie1")),
                                    NaPrzelewie2 = reader.IsDBNull(reader.GetOrdinal("NaPrzelewie2")) ? "" : reader.GetString(reader.GetOrdinal("NaPrzelewie2")),
                                    Skrot = reader.IsDBNull(reader.GetOrdinal("Skrot")) ? "" : reader.GetString(reader.GetOrdinal("Skrot")),
                                    Ulica = reader.IsDBNull(reader.GetOrdinal("Ulica")) ? "" : reader.GetString(reader.GetOrdinal("Ulica")),
                                    Kod = reader.IsDBNull(reader.GetOrdinal("Kod")) ? "" : reader.GetString(reader.GetOrdinal("Kod")),
                                    Miasto = reader.IsDBNull(reader.GetOrdinal("Miasto")) ? "" : reader.GetString(reader.GetOrdinal("Miasto")),
                                    Telefon = reader.IsDBNull(reader.GetOrdinal("Telefon")) ? "" : reader.GetString(reader.GetOrdinal("Telefon")),
                                    Fax = reader.IsDBNull(reader.GetOrdinal("Fax")) ? "" : reader.GetString(reader.GetOrdinal("Fax")),
                                    EMail = reader.IsDBNull(reader.GetOrdinal("EMail")) ? "" : reader.GetString(reader.GetOrdinal("EMail")),
                                    Bank = reader.IsDBNull(reader.GetOrdinal("Bank")) ? "" : reader.GetString(reader.GetOrdinal("Bank")),
                                    Konto = reader.IsDBNull(reader.GetOrdinal("Konto")) ? "" : reader.GetString(reader.GetOrdinal("Konto")),
                                    NIP = reader.IsDBNull(reader.GetOrdinal("NIP")) ? "" : reader.GetString(reader.GetOrdinal("NIP")),
                                    Staly = reader.IsDBNull(reader.GetOrdinal("Staly")) ? 0 : reader.GetInt16(reader.GetOrdinal("Staly")),
                                    Dostawca = reader.IsDBNull(reader.GetOrdinal("Dostawca")) ? 0 : reader.GetInt16(reader.GetOrdinal("Dostawca")),
                                    RabatDost = reader.IsDBNull(reader.GetOrdinal("RabatDost")) ? 0 : reader.GetDecimal(reader.GetOrdinal("RabatDost")),
                                    FormaPlatDost = reader.IsDBNull(reader.GetOrdinal("FormaPlatDost")) ? 0 : reader.GetInt16(reader.GetOrdinal("FormaPlatDost")),
                                    TermPlatDost = reader.IsDBNull(reader.GetOrdinal("TermPlatDost")) ? 0 : reader.GetInt16(reader.GetOrdinal("TermPlatDost")),
                                    CzasRealZam = reader.IsDBNull(reader.GetOrdinal("CzasRealZam")) ? 0 : reader.GetInt16(reader.GetOrdinal("CzasRealZam")),
                                    Producent = reader.IsDBNull(reader.GetOrdinal("Producent")) ? 0 : reader.GetInt16(reader.GetOrdinal("Producent")),
                                    Odbiorca = reader.IsDBNull(reader.GetOrdinal("Odbiorca")) ? 0 : reader.GetInt16(reader.GetOrdinal("Odbiorca")),
                                    RabatOdb = reader.IsDBNull(reader.GetOrdinal("RabatOdb")) ? 0 : reader.GetDecimal(reader.GetOrdinal("RabatOdb")),
                                    FormaPlatOdb = reader.IsDBNull(reader.GetOrdinal("FormaPlatOdb")) ? 0 : reader.GetInt16(reader.GetOrdinal("FormaPlatOdb")),
                                    TermPlatOdb = reader.IsDBNull(reader.GetOrdinal("TermPlatOdb")) ? 0 : reader.GetInt16(reader.GetOrdinal("TermPlatOdb")),
                                    MaxKredyt = reader.IsDBNull(reader.GetOrdinal("MaxKredyt")) ? 0 : reader.GetDecimal(reader.GetOrdinal("MaxKredyt")),
                                    KodKarty = reader.IsDBNull(reader.GetOrdinal("KodKarty")) ? "" : reader.GetString(reader.GetOrdinal("KodKarty")),
                                    KartaAktywna = reader.IsDBNull(reader.GetOrdinal("KartaAktywna")) ? 0 : reader.GetInt16(reader.GetOrdinal("KartaAktywna")),
                                    TermWaznKarty = reader.IsDBNull(reader.GetOrdinal("TermWaznKarty")) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("TermWaznKarty")),
                                    PoziomRabatu = reader.IsDBNull(reader.GetOrdinal("PoziomRabatu")) ? 0 : reader.GetInt16(reader.GetOrdinal("PoziomRabatu")),
                                    NrAnalityki = reader.IsDBNull(reader.GetOrdinal("NrAnalityki")) ? "" : reader.GetString(reader.GetOrdinal("NrAnalityki")),
                                    KodKontr = reader.IsDBNull(reader.GetOrdinal("KodKontr")) ? "" : reader.GetString(reader.GetOrdinal("KodKontr")),
                                    ShopNumber = config.ShopNumber,
                                    CompanyId = config.CompanyId
                                });
                            }

                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                connection.Close();
            }


            return result;
        }
    }
}
