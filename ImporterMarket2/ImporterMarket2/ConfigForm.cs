﻿using Model.DataTransfer;
using Model.Domain;
using Model.Domain.PCMarket;
using System;
using System.Windows.Forms;

namespace ImporterMarket2
{
    public partial class ConfigForm : Form
    {
        string ConfigFile;
        private readonly PCMImportService _pcmImportService;
        public ConfigForm()
        {
            InitializeComponent();
            ConfigFile = System.Environment.CurrentDirectory + "\\" + "ConfigImporter";
            _pcmImportService = new PCMImportService();
            FillConfigView();
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            var config = new ConfigDto();
            FillConfigDto(config);
            var sqlService = new SQLService();
            try
            {
                sqlService.CheckSQLConnection(config);
                MessageBox.Show("OK");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            var config = new ConfigDto();
            FillConfigDto(config);
            ConfigWriter.WriteToJsonFile(ConfigFile, config);
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            var config = new ConfigDto();
            FillConfigDto(config);
            var connection = ConfigWriter.CreateMQConnection(config);
            try
            {
                connection.CreateConnection();
                MessageBox.Show("OK");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var config = new ConfigDto();
            FillConfigDto(config);
            ConfigWriter.WriteToJsonFile(ConfigFile, config);
        }

        public class ComboboxItem
        {
            public string Text { get; set; }
            public object Value { get; set; }
        }
        private void FillConfigDto(ConfigDto config)
        {
            config.HostName = textBox1.Text;
            config.UserName = textBox2.Text;
            config.Pasword = textBox3.Text;
            config.VirtualHost = textBox4.Text;
            config.Port = int.Parse(textBox5.Text == "" ? "0" : textBox5.Text);
            config.RequestedConnectionTimeout = int.Parse(textBox6.Text == "" ? "0" : textBox6.Text);
            config.RequestedHeartbeat = int.Parse(textBox7.Text == "" ? "0" : textBox7.Text);
            config.AutomaticRecoveryEnabled = checkBox1.Checked;

            config.SQLServerName = textBox14.Text;
            config.SQLUsername = textBox13.Text;
            config.SQLPassword = textBox12.Text;
            config.SQLSecurity = comboBox1.SelectedIndex == 0 ? "SQL" : "Windows";
            config.SQLName = textBox8.Text;

            config.ShopNumber = textBox10.Text;
            config.CompanyId = textBox9.Text;
        }

        private void configTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillConfigView();
        }

        private void FillConfigView()
        {
            try
            {
                var config = ConfigWriter.ReadFromJsonFile<ConfigDto>(ConfigFile);

                comboBox1.Items.Clear();
                comboBox1.Items.Add(new ComboboxItem() { Text = "SQL", Value = 1 });
                comboBox1.Items.Add(new ComboboxItem() { Text = "Windows Authentication", Value = 1 });
                comboBox1.DisplayMember = "Text";
                comboBox1.ValueMember = "Value";
                comboBox1.SelectedIndex = 0;


                textBox1.Text = config.HostName;
                textBox2.Text = config.UserName;
                textBox3.Text = config.Pasword;
                textBox4.Text = config.VirtualHost;
                textBox5.Text = config.Port.ToString();
                textBox6.Text = config.RequestedConnectionTimeout.ToString();
                textBox7.Text = config.RequestedHeartbeat.ToString();
                checkBox1.Checked = config.AutomaticRecoveryEnabled;

                textBox14.Text = config.SQLServerName;
                textBox13.Text = config.SQLUsername;
                textBox12.Text = config.SQLPassword;
                comboBox1.SelectedIndex = config.SQLSecurity == "SQL" ? 0 : 1;
                textBox8.Text = config.SQLName;

                textBox10.Text = config.ShopNumber.ToString();
                textBox9.Text = config.CompanyId.ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Trying to create new config file.");
                var config = new ConfigDto();
                FillConfigDto(config);
                ConfigWriter.WriteToJsonFile(ConfigFile, config);
                FillConfigView();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportTowar();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportAsort();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportJM();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportStany();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportDok();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            //_pcmImportService.ImportPozDok();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportDokKontr();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportDokKurs();
        }

        private void button13_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportTekstDok();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportTekstPoz();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportVat();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportUzytkownik();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportRozlicza();
        }

        private void button18_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportRozbicieDok();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportKontrahent();
        }

        private void button20_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportTowar();
            _pcmImportService.ImportAsort();
            _pcmImportService.ImportJM();
            _pcmImportService.ImportStany();
        }

        private void button21_Click(object sender, EventArgs e)
        {
            _pcmImportService.ImportDok();
            //_pcmImportService.ImportPozDok();
            _pcmImportService.ImportDokKontr();
            _pcmImportService.ImportDokKurs();
            _pcmImportService.ImportTekstDok();
            _pcmImportService.ImportTekstPoz();
            _pcmImportService.ImportVat();
            _pcmImportService.ImportUzytkownik();
            _pcmImportService.ImportRozlicza();
            _pcmImportService.ImportRozbicieDok();
            _pcmImportService.ImportKontrahent();
        }

        private void button22_Click(object sender, EventArgs e)
        {
            var config = new ConfigDto();
            FillConfigDto(config);
            ConfigWriter.WriteToJsonFile(ConfigFile, config);
        }

        private void ConfigForm_Load(object sender, EventArgs e)
        {

        }
    }
}
